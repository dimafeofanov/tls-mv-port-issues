//=============================================================================
// Yanfly Engine Plugins - Battle Status Window and Enhanced TP shim
// YEP_BattleStatusWindowDrawFix.js
//=============================================================================
//=============================================================================
 /*:
 * @plugindesc v0.1 A fix for drawing TP / MP for actors with only one.
 *
 *
 * @author Teve
 * @help
 * Does what it says on the tin
 * Place directly under BattleStatusWindow
 */

Window_BattleStatus.prototype.drawGaugeArea = function(rect, actor) {
   this.contents.fontSize = Yanfly.Param.BSWParamFontSize;
   this._enableYBuffer = true;
   var wy = rect.y + rect.height - this.lineHeight();
   var wymod = (Imported.YEP_CoreEngine) ? Yanfly.Param.GaugeHeight : 6;
   var wymod = Math.max(16, wymod);
   this.drawActorHp(actor, rect.x, wy - wymod, rect.width);
   if (this.getGaugesDrawn(actor) <= 2) {
     this.drawActorMp(actor, rect.x, wy, rect.width);
   } else {
if (actor.mmp == 0){
this.drawActorTp (actor, rect.x, wy, rect.width);
}else if ($gameActors._data[actor.actorId()]._tpMode == 1){
this.drawActorMp (actor, rect.x, wy, rect.width);
}else {
     var ww = rect.width / 2;
     this.drawActorMp(actor, rect.x, wy, ww);
     this.drawActorTp(actor, rect.x + ww, wy, ww);
}
   }
   this._enableYBuffer = false;
};