//=============================================================================
// Shim for YEP_X_Autosave
// YEP_X_Autosave_shim.js
//=============================================================================
//=============================================================================
/*:
 * @plugindesc v.01 Shim to make the game ignore slot 100.
 * @author Teve
 * 
 * @help
 * //=============================================================================
 * //Background
 * //=============================================================================
 * This will make the game ignore slot 100 when its been saved to and loaded from
 * You can still save to it manually, but why would you?
 * 
 */
//=============================================================================
var autosaveid = 100;
DataManager.loadGameWithoutRescue = function(savefileId) {
    var globalInfo = this.loadGlobalInfo();
    if (this.isThisGameFile(savefileId)) {
        var json = StorageManager.load(savefileId);
        this.createGameObjects();
        this.extractSaveContents(JsonEx.parse(json));
        if (savefileId != autosaveid) {
        this._lastAccessedId = savefileId;
        }
        return true;
    } else {
        return false;
    }
};
DataManager.saveGameWithoutRescue = function(savefileId) {
    var json = JsonEx.stringify(this.makeSaveContents());
    if (json.length >= 200000) {
        console.warn('Save data too big!');
    }
    StorageManager.save(savefileId, json);
    if (savefileId != autosaveid) {
        this._lastAccessedId = savefileId;
        }
    var globalInfo = this.loadGlobalInfo() || [];
    globalInfo[savefileId] = this.makeSavefileInfo();
    this.saveGlobalInfo(globalInfo);
    return true;
};
StorageManager.getCurrentAutosaveSlot = function() {
    return autosaveid;
  };
  
  StorageManager.setCurrentAutosaveSlot = function(savefileId) {
    this._currentAutosaveSlot = autosaveid;
  };

  DataManager.latestSavefileId = function() {
    var globalInfo = this.loadGlobalInfo();
    var savefileId = 1;
    var timestamp = 0;
    if (globalInfo) {
        for (var i = 1; i < (globalInfo.length -1); i++) {
            if (this.isThisGameFile(i) && globalInfo[i].timestamp > timestamp) {
                timestamp = globalInfo[i].timestamp;
                savefileId = i;
            }
        }
    }
    return savefileId;
};