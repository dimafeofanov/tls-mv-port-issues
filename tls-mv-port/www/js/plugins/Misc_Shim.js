// ============================================
// Misc Shim for future compatibility
// Misc_Shim.js
// ============================================

//=============================================================================
/*:
 * @plugindesc v0.0.1 Assorted shims for various script calls
 * @author Teven
 * 
 * @help
 * //=============================================================================
 * //Background
 * //=============================================================================
 * 
 * In the event that versioning isn't super smooth, I effectively aliased most
 * of the standard calls used from the VX ACE version to use plugin commands for
 * a MV replacement.  The only thing not covered is quest completion, as the script
 * version checks against a list of prime objectives.  The JS version, has no such
 * list and it would be a larger timesink to build such a thing then it would be to
 * manually complete quests in the maps themselves when then situation calls for it.
 * //=============================================================================
 */
function require_actor(x){
    CallPluginCommand(`RequireActor ${x}`)
}
function lock_actor(x){
    CallPluginCommand(`LockActor ${x}`)
}
function unrequire_actor(x){
    CallPluginCommand(`UnrequireActor ${x}`)
}
function unlock_actor(x){
    CallPluginCommand(`UnlockActor ${x}`)
}
function change_tp_mode(x,y){
    y = y + 1
    CallPluginCommand(`ChangeTpMode Actor ${x} to ${y}`)
}
function reveal_objective(x){
   // iterate through arguments
   for (var i = 1; i < arguments.length; i++) {
        arguments[i] = arguments[i] + 1
        if (arguments[i] == 1){
            CallPluginCommand(`Quest Add ${x}`)
            console.log(`CallPluginCommand(Quest Add ${x})`)
            CallPluginCommand(`Quest ${x} Add Objective ${arguments[i]}`)
            console.log(`CallPluginCommand(Quest ${x} Add Objective ${i})`)
        }else{
            CallPluginCommand(`Quest ${x} Add Objective ${arguments[i]}`)
            console.log(`CallPluginCommand(Quest ${x} Add Objective ${i})`)
        }
    }
}

function complete_objective(x){
       // iterate through arguments
       for (var i = 1; i < arguments.length; i++) {
            arguments[i] = arguments[i] + 1
            CallPluginCommand(`Quest ${x} Complete Objective ${arguments[i]}`)
            console.log(`CallPluginCommand(Quest ${x} Complete Objective ${i})`)
        }
}

function fail_objective(x){
    // iterate through arguments
    for (var i = 1; i < arguments.length; i++) {
        arguments[i] = arguments[i] + 1
        CallPluginCommand(`Quest ${x} Fail Objective ${arguments[i]}`)
        console.log(`CallPluginCommand(Quest ${x} Fail Objective ${i})`)
        }
     }
function delete_quest(x){
    CallPluginCommand(`Quest Remove ${x}`)
    console.log(`CallPluginCommand(Quest Remove ${x})`)
}

function fail_quest(x){
    CallPluginCommand(`Quest Set Failed ${x}`)
    console.log(`CallPluginCommand(Quest Set Failed ${x})`)
}

function complete_quest(x){
    CallPluginCommand(`Quest Set Completed ${x}`)
    console.log(`CallPluginCommand(Quest Set Completed ${x})`)
}
