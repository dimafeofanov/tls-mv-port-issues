//=============================================================================
// Teve's Terrible Tile Swap
// tilestest.js
//=============================================================================
//=============================================================================
/*:
 * @plugindesc v.01 Super terrible tile_swap trash put together in an afternoon.
 * @author Teve
 * 
 * @help
 * //=============================================================================
 * //Background
 * //=============================================================================
 * This garbage I threw together so I could get tile swapping working for the HQ.
 * For all intents and purposes its a very stripped down and specialized port
 * of HimeWorks tile_swap, except without most of the extra functionality.
 * Its kind of hacky, and might break under certain circumstances.  I think I
 * worked out all the conditional calls via an extra parallel event tied to a
 * self-switch in each affected area.  Doesn't seem to have any noticable 
 * perfomance effects. 
 */
//=============================================================================
 //Converts an inputted tile to the numerical equivalent
function convert_tid(tileID){
    var tileID = tileID.split(/(\D)/);
    tileID.shift();
    var page = tileID[0].toUpperCase();
    var tid = tileID[1] -1;
    if (page == 'A'){
        if (tid < 128) {
            return (tid * 48 + 2048);
        }else{
            return (tid - 128 + 1536);
        }
    }
    if (page == 'B'){
        return tid ;
     }
    if (page == 'C'){
		return (tid + 256);
    }
    if (page == 'D'){
        return (tid + 512) ;
    }
    if (page == 'E'){
		return (tid + 768) ;
    }
}

    
//Does the swapping.  Possibly unsafe if interpreter is doing stuff.    
    function tile_swap(oldtid, newtid){
        var oldtid = convert_tid(oldtid)
        var newtid = convert_tid(newtid)
        var oldautotile = ((oldtid - 2048) / 48)
    var newautotile = ((newtid - 2048) / 48)
    if (oldautotile < 0){
        var index = 0
    }
    if (!(index)){
        newindex = [0,1,2,3,4,5,6,7,8,9,10,12,13,16,17,18,19,20,21,22,23,24,25,26,27,28,29,30,31,32,33,34,35,36,37,38,39,40,41,42,43,44,45,46]
        newindex.forEach(function(i){
            //console.log("Loop "+ i)
            newtid = (2048 + (48 * newautotile)+ parseInt(i))
            oldtid = (2048 + (48 * oldautotile)+ parseInt(i))
            var k = $dataMap.data.indexOf(oldtid)
            while (k != -1){
                //console.log($dataMap.data[k])
                $dataMap.data[k] = newtid
                k = $dataMap.data.indexOf(oldtid)
            }
        })
    }else{
    var k = $dataMap.data.indexOf(oldtid)
    while (k != -1){
        $dataMap.data[k] = newtid
        k = $dataMap.data.indexOf(oldtid)
        }
    }
}
