/*:
 * @plugindesc v.01 Shim to allow for zero size parties.
 * @author Teve

 * @help
 * //=============================================================================
 * //Background
 * //=============================================================================
 * A modification to HimePartyScene allowing you to move every character to
 * a party, leaving the base party empty.  Under normal circumstances that'd
 * break being on the map, I'm working around that with a call to addActor post
 * the scene, and a removeActor when calling it up again.  This allows for Simon
 * to be visible while assigning party members to groups.
 */
//=============================================================================
//Shim to allow for 0 size parties
Scene_PartySwitch.prototype.onPartyCancel = function() {
      this.popScene();
  };