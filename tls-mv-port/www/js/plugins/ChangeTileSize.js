//=============================================================================
// Change Tile Size
// by Shaz
// version 1.2
// Last Update: 2017.04.12
//=============================================================================

/*:
 * @plugindesc Allows maps based on a grid size other than 48x48
   <ChangeTileSize> v1.1
 * @author Shaz
 *
 * @param Tile Size
 * @desc Size of tiles (pixels)
 * @default 48
 *
 * @param Tileset Image Folder
 * @desc Folder where the in-game tilesets are kept
 * @default img/tilesets/
 *
 * @param Parallax Image Folder
 * @desc Folder where the in-game parallaxes are kept
 * @default img/parallaxes/
 *
 * @help This plugin does not provide plugin commands.
 *
 * To use the map editor with tiles of a size other than 48x48 in your project,
 * have two folders for tileset images.  The folder named in the plugin
 * parameters is for original, high-quality, final-sized tiles.  The
 * original folder, img/tilesets/ can contain a duplicate copy of each
 * tileset, shrunk or enlarged to 48x48 pixels per tile.  Quality in the
 * editor may be poorer, but the original tiles will be used in the game.
 * The img/tilesets folder can be cleared prior to distribution.
 *
 * The same applies to the img/parallaxes folder if using a parallax map
 * with a grid size other than 48x48.
 *
 * ----------------------------------------------------------------------------
 * Revisions
 *
 * 1.1  2016.10.17 Catered for Tilemap tilesize to reduce choppiness
 *                 Removed dependency on js file name
 *      2016.10.23 Ensure resized tiles/parallaxes are included in build
 * 1.2  2017.04.12 Fix issue with excluding unused resources
 * ----------------------------------------------------------------------------
RESOURCE LIST - DO NOT MODIFY BELOW THIS LINE
 * @requiredAssets img/parallaxesgame/StarSky
 * @requiredAssets img/parallaxesgame/IncubusBG Small
 * @requiredAssets img/parallaxesgame/StarlitRainbow
 * @requiredAssets img/parallaxesgame/CloudySky2
 * @requiredAssets img/parallaxesgame/StarlitPurple
 * @requiredAssets img/parallaxesgame/StarlitDark
 * @requiredAssets img/parallaxesgame/StarlitGreen
 * @requiredAssets img/parallaxesgame/StarlitRed
 * @requiredAssets img/parallaxesgame/StarlitDesert
 * @requiredAssets img/parallaxesgame/StarlitWhite
 * @requiredAssets img/parallaxesgame/StarlitSky
 * @requiredAssets img/parallaxesgame/Mountains2
 * @requiredAssets img/parallaxesgame/mapardford
 * @requiredAssets img/parallaxesgame/maparclent
 * @requiredAssets img/parallaxesgame/maparam
 * @requiredAssets img/parallaxesgame/BlueSky
 * @requiredAssets img/parallaxesgame/CloudySky1
 * @requiredAssets img/parallaxesgame/fheliel race
 * @requiredAssets img/parallaxesgame/DarkSpace1
 * @requiredAssets img/parallaxesgame/DarkSpace2
 * @requiredAssets img/parallaxesgame/SeaofClouds
 * @requiredAssets img/parallaxesgame/Mountains4
 * @requiredAssets img/tilesetsgame/GermaniaBInt
 * @requiredAssets img/tilesetsgame/GermaniaA5Int
 * @requiredAssets img/tilesetsgame/GermaniaA4Int
 * @requiredAssets img/tilesetsgame/GermaniaA2Int
 * @requiredAssets img/tilesetsgame/WindShrine
 * @requiredAssets img/tilesetsgame/Astral_A5-2
 * @requiredAssets img/tilesetsgame/Sam TileA2-3
 * @requiredAssets img/tilesetsgame/GermaniaE
 * @requiredAssets img/tilesetsgame/GermaniaD
 * @requiredAssets img/tilesetsgame/GermaniaC
 * @requiredAssets img/tilesetsgame/GermaniaB
 * @requiredAssets img/tilesetsgame/GermaniaA5
 * @requiredAssets img/tilesetsgame/GermaniaA4
 * @requiredAssets img/tilesetsgame/GermaniaA3
 * @requiredAssets img/tilesetsgame/GermaniaA2
 * @requiredAssets img/tilesetsgame/GermaniaA1
 * @requiredAssets img/tilesetsgame/IncubusKing2
 * @requiredAssets img/tilesetsgame/IncubusKing
 * @requiredAssets img/tilesetsgame/Sam TileA2-2
 * @requiredAssets img/tilesetsgame/Castle_E
 * @requiredAssets img/tilesetsgame/MagicTiles
 * @requiredAssets img/tilesetsgame/AstralCastle_A2-2
 * @requiredAssets img/tilesetsgame/Rainforest
 * @requiredAssets img/tilesetsgame/AllinOne
 * @requiredAssets img/tilesetsgame/Holy_E
 * @requiredAssets img/tilesetsgame/Holy_A5
 * @requiredAssets img/tilesetsgame/Astral_A4b
 * @requiredAssets img/tilesetsgame/AstralCastle_A4
 * @requiredAssets img/tilesetsgame/AstralCastle_A2
 * @requiredAssets img/tilesetsgame/Astral_E
 * @requiredAssets img/tilesetsgame/Astral_A5
 * @requiredAssets img/tilesetsgame/Astral_A4
 * @requiredAssets img/tilesetsgame/Astral_A2
 * @requiredAssets img/tilesetsgame/Astral_A1
 * @requiredAssets img/tilesetsgame/Lun TileB Int
 * @requiredAssets img/tilesetsgame/Stenai Int
 * @requiredAssets img/tilesetsgame/MACK_tilee2
 * @requiredAssets img/tilesetsgame/MACK_tiled2
 * @requiredAssets img/tilesetsgame/Stenai Ext
 * @requiredAssets img/tilesetsgame/Outside_A2b
 * @requiredAssets img/tilesetsgame/pavilion
 * @requiredAssets img/tilesetsgame/Lun TileB Ext
 * @requiredAssets img/tilesetsgame/Lun TileC Green
 * @requiredAssets img/tilesetsgame/Lun TileC Brown
 * @requiredAssets img/tilesetsgame/Lun TileC Blue
 * @requiredAssets img/tilesetsgame/Inside_A5 Mod
 * @requiredAssets img/tilesetsgame/Inside_A4 Mod
 * @requiredAssets img/tilesetsgame/Inside_A2 Mod
 * @requiredAssets img/tilesetsgame/Rural_TileE
 * @requiredAssets img/tilesetsgame/Rural_TileD
 * @requiredAssets img/tilesetsgame/Rural_TileC
 * @requiredAssets img/tilesetsgame/Rural_TileB
 * @requiredAssets img/tilesetsgame/statues
 * @requiredAssets img/tilesetsgame/egypt_desert_pandamaru
 * @requiredAssets img/tilesetsgame/evilcastle_C2
 * @requiredAssets img/tilesetsgame/Exterior_Arabian_TileA5
 * @requiredAssets img/tilesetsgame/Exterior_Arabian_TileB
 * @requiredAssets img/tilesetsgame/tree huge light
 * @requiredAssets img/tilesetsgame/woods_pandamaru
 * @requiredAssets img/tilesetsgame/World_A2alt
 * @requiredAssets img/tilesetsgame/tree interior
 * @requiredAssets img/tilesetsgame/Elven Forests E
 * @requiredAssets img/tilesetsgame/Elven Forests D
 * @requiredAssets img/tilesetsgame/Elven Forests C
 * @requiredAssets img/tilesetsgame/Elven Forests A5
 * @requiredAssets img/tilesetsgame/Elven Forests A2
 * @requiredAssets img/tilesetsgame/Elven Forests A1
 * @requiredAssets img/tilesetsgame/Elven Forests B
 * @requiredAssets img/tilesetsgame/Macks_Tile_E
 * @requiredAssets img/tilesetsgame/arabic_pandamaru
 * @requiredAssets img/tilesetsgame/ArabianNights-TileC
 * @requiredAssets img/tilesetsgame/ArabianNights-TileB
 * @requiredAssets img/tilesetsgame/ArabianNights-TileA5
 * @requiredAssets img/tilesetsgame/ArabianNights-TileA4
 * @requiredAssets img/tilesetsgame/BaseVariants
 * @requiredAssets img/tilesetsgame/BaseBeds
 * @requiredAssets img/tilesetsgame/Pandaext B
 * @requiredAssets img/tilesetsgame/City_TileE
 * @requiredAssets img/tilesetsgame/pand garden
 * @requiredAssets img/tilesetsgame/City_TileE-2
 * @requiredAssets img/tilesetsgame/City_TileD
 * @requiredAssets img/tilesetsgame/City_TileC
 * @requiredAssets img/tilesetsgame/City_TileB
 * @requiredAssets img/tilesetsgame/pandaroyal
 * @requiredAssets img/tilesetsgame/pandalib
 * @requiredAssets img/tilesetsgame/Pandaint C
 * @requiredAssets img/tilesetsgame/Pandaint B
 * @requiredAssets img/tilesetsgame/Tana01(vx)_mapcip
 * @requiredAssets img/tilesetsgame/AIN_Obelisks
 * @requiredAssets img/tilesetsgame/TileE_VX
 * @requiredAssets img/tilesetsgame/AIN_BedsSet
 * @requiredAssets img/tilesetsgame/Gothic_Addon_byLunarea4
 * @requiredAssets img/tilesetsgame/RoyalTileset-TileC
 * @requiredAssets img/tilesetsgame/RoyalTileset-TileB
 * @requiredAssets img/tilesetsgame/RoyalTileset-TileA5
 * @requiredAssets img/tilesetsgame/celiannatilea4
 * @requiredAssets img/tilesetsgame/RoyalTileset-OuterTileA5
 * @requiredAssets img/tilesetsgame/RoyalTileset-TileA2
 * @requiredAssets img/tilesetsgame/RoyalTileset-TileA1
 * @requiredAssets img/tilesetsgame/oio86
 * @requiredAssets img/tilesetsgame/A5 Makin RTP Magic Tileset 
 * @requiredAssets img/tilesetsgame/A4 Makin RTP Magic Tileset 
 * @requiredAssets img/tilesetsgame/A2 Makin RTP Magic Tileset 
 * @requiredAssets img/tilesetsgame/Makin RTP Magic Tileset (B-E)
 * @requiredAssets img/tilesetsgame/AN-Extra-Camel Tent
 * @requiredAssets img/tilesetsgame/autum_pandamaru
 * @requiredAssets img/tilesetsgame/autumn2
 * @requiredAssets img/tilesetsgame/autum_A5
 * @requiredAssets img/tilesetsgame/autum_A4
 * @requiredAssets img/tilesetsgame/autum_A2
 * @requiredAssets img/tilesetsgame/autum_A1
 * @requiredAssets img/tilesetsgame/evilcastle_A2
 * @requiredAssets img/tilesetsgame/Haunted_Forest_byLunarea.png~original
 * @requiredAssets img/tilesetsgame/evilcastle_D
 * @requiredAssets img/tilesetsgame/evilcastle_C
 * @requiredAssets img/tilesetsgame/evilcastle_B
 * @requiredAssets img/tilesetsgame/evilcastle_A5
 * @requiredAssets img/tilesetsgame/evilcastle_A4
 * @requiredAssets img/tilesetsgame/A-Haunted_byLunarea.png~original
 * @requiredAssets img/tilesetsgame/evilcastle_A1
 * @requiredAssets img/tilesetsgame/MACK_tilec
 * @requiredAssets img/tilesetsgame/MACK_tileb
 * @requiredAssets img/tilesetsgame/MACK_tilea2
 * @requiredAssets img/tilesetsgame/sex toys
 * @requiredAssets img/tilesetsgame/japanese_style_by_nicnubill-d6ppkhp
 * @requiredAssets img/tilesetsgame/Sam TileD
 * @requiredAssets img/tilesetsgame/Sam TileC
 * @requiredAssets img/tilesetsgame/Sam TileB
 * @requiredAssets img/tilesetsgame/Sam TileA5
 * @requiredAssets img/tilesetsgame/Sam TileA4
 * @requiredAssets img/tilesetsgame/Sam TileA3
 * @requiredAssets img/tilesetsgame/Sam TileA2
 * @requiredAssets img/tilesetsgame/Sam TileA1
 * @requiredAssets img/tilesetsgame/MACK_tilee
 * @requiredAssets img/tilesetsgame/MACK_tiled
 * @requiredAssets img/tilesetsgame/Death theme B-E
 * @requiredAssets img/tilesetsgame/halloween_forest_ByPandaMaru
 * @requiredAssets img/tilesetsgame/MACK_tilea5
 * @requiredAssets img/tilesetsgame/MACK_tilea4
 * @requiredAssets img/tilesetsgame/MACK_tilea3
 * @requiredAssets img/tilesetsgame/autumn1
 * @requiredAssets img/tilesetsgame/MACK_tilea1
 * @requiredAssets img/tilesetsgame/Dungeon_C
 * @requiredAssets img/tilesetsgame/Dungeon_A5
 * @requiredAssets img/tilesetsgame/Dungeon_A4
 * @requiredAssets img/tilesetsgame/Dungeon_A2
 * @requiredAssets img/tilesetsgame/Dungeon_A1
 * @requiredAssets img/tilesetsgame/FancyWindows
 * @requiredAssets img/tilesetsgame/Dungeon_B
 * @requiredAssets img/tilesetsgame/Tree Variants
 * @requiredAssets img/tilesetsgame/Outside_C
 * @requiredAssets img/tilesetsgame/Outside_B
 * @requiredAssets img/tilesetsgame/Outside_A5
 * @requiredAssets img/tilesetsgame/Outside_A4
 * @requiredAssets img/tilesetsgame/Outside_A3
 * @requiredAssets img/tilesetsgame/Outside_A2
 * @requiredAssets img/tilesetsgame/Outside_A1
 * @requiredAssets img/tilesetsgame/field d
 * @requiredAssets img/tilesetsgame/field c
 * @requiredAssets img/tilesetsgame/World_B
 * @requiredAssets img/tilesetsgame/World_A2
 * @requiredAssets img/tilesetsgame/World_A1
 * @requiredAssets img/tilesetsgame/TileE
 * @requiredAssets img/tilesetsgame/tent
 * @requiredAssets img/tilesetsgame/Inside_C
 * @requiredAssets img/tilesetsgame/Inside_B
 * @requiredAssets img/tilesetsgame/Inside_A5
 * @requiredAssets img/tilesetsgame/Inside_A4
 * @requiredAssets img/tilesetsgame/Inside_A2
 * @requiredAssets img/tilesetsgame/Inside_A1
 * @requiredAssets
RESOURCE LIST - DO NOT MODIFY ABOVE THIS LINE
 */

(function() {

  var parameters = $plugins.filter(function(p) { return p.description.contains('<ChangeTileSize>'); })[0].parameters;
  var tileSize = parseInt(parameters['Tile Size'] || 48);
  var tilesetsFolder = String(parameters['Tileset Image Folder'] || 'img/tilesets/');
  var parallaxesFolder = String(parameters['Parallax Image Folder'] || 'img/parallaxes/');

  ImageManager.loadTileset = function(filename, hue) {
    return this.loadBitmap(tilesetsFolder, filename, hue, false);
  };

  ImageManager.loadParallax = function(filename, hue) {
      return this.loadBitmap(parallaxesFolder, filename, hue, true);
  };

  Game_Map.prototype.tileWidth = function() {
    return tileSize;
  };

  Game_Map.prototype.tileHeight = function() {
    return tileSize;
  };

  Game_Vehicle.prototype.maxAltitude = function() {
    return tileSize;
  };

  var Tilemap_initialize = Tilemap.prototype.initialize;
  Tilemap.prototype.initialize = function() {
    Tilemap_initialize.call(this);
    this._tileWidth = tileSize;
    this._tileHeight = tileSize;
  };

  // Add image files to required assets list, to ensure they are included
  // in a deployed project
  var _changeTileSize_Scene_Boot_start = Scene_Boot.prototype.start;
  Scene_Boot.prototype.start = function() {
    _changeTileSize_Scene_Boot_start.call(this);

    if (!Utils.isOptionValid('test') || DataManager.isBattleTest() ||
      DataManager.isEventTest()) {
        return;
      }

    // Read in this script
    var fs = require('fs');
    var path = window.location.pathname.replace(/(\/www|)\/[^\/]*$/, '/');
    if (path.match(/^\/([A-Z]\:)/)) {
      path = path.slice(1);
    }
    path = decodeURIComponent(path);
    var scriptName = $plugins.filter(function(p) {
      return p.description.contains('<ChangeTileSize>') && p.status;
    })[0].name + '.js';
    var sourcePath = path + 'js/plugins/' + scriptName;
    var dataPath = path + 'data/';
    if (fs.existsSync(sourcePath)) {
      var source = fs.readFileSync(sourcePath, { encoding: 'utf8' });
      source = source.split('\r\n');
      var resListStart = source.indexOf('RESOURCE LIST - DO NOT MODIFY BELOW THIS LINE');
      var resListEnd = source.indexOf('RESOURCE LIST - DO NOT MODIFY ABOVE THIS LINE');
      if (resListStart === -1 || resListEnd === -1) {
        console.log('ERROR - Unable to create resource list for ' + scriptName);
        console.log('Please redownload the script and paste over your current version');
      } else {
        // remove previous list of resources
        source.splice(resListStart + 1, resListEnd - resListStart - 1);
        // build list of new resources
        var resTilesets = [];
        var resParallaxes = [];

        // get tilesets & parallaxes that are actually used
        $dataMapInfos.forEach(function(mapinfo) {
          if (mapinfo) {
            var map = JSON.parse(fs.readFileSync(dataPath + 'Map%1.json'.format(mapinfo.id.padZero(3))));

            if (!resTilesets.contains(map.tilesetId)) {
              resTilesets.push(map.tilesetId);
            }

            if (map.parallaxName !== '' && !resParallaxes.contains(map.parallaxName)) {
              resParallaxes.push(map.parallaxName);
            }

            // map events
            map.events.forEach(function (event) {
              if (event) {
                event.pages.forEach(function (page) {
                  if (page) {
                    getResources(page.list, resTilesets, resParallaxes);
                  }
                })
              }
            })
          }
        });

        // common events
        $dataCommonEvents.forEach(function(commonEvent) {
          if (commonEvent) {
            getResources(commonEvent.list, resTilesets, resParallaxes);
          }
        });

        // troop events
        $dataTroops.forEach(function (troop) {
          if (troop) {
            troop.pages.forEach(function (page) {
              if (page) {
                getResources(page.list, resTilesets, resParallaxes);
              }
            })
          }
        })

        // add resource list to script
        var inspos = resListStart + 1;

        resTilesetNames = [];
        resTilesets.forEach(function(tilesetId) {
          var tileset = $dataTilesets[tilesetId];
          if (tileset) {
            tileset.tilesetNames.forEach(function(tilesetName) {
              if (tilesetName !== '' && !resTilesetNames.contains(tilesetName)) {
                resTilesetNames.push(tilesetName);
              }
            })
          }
        });

        var tag = ' * @requiredAssets';
        source.splice(inspos, 0, tag);

        resTilesetNames.forEach(function(tilesetName) {
          tag = ' * @requiredAssets ' + tilesetsFolder + tilesetName;
          source.splice(inspos, 0, tag);
        });

        resParallaxes.forEach(function(parallaxName) {
          tag = ' * @requiredAssets ' + parallaxesFolder + parallaxName;
          source.splice(inspos, 0, tag);
        });

        // and save the file
        source = source.join('\r\n');
        fs.writeFileSync(sourcePath, source, { encoding: 'utf8' });
      }
    }
  };

  getResources = function(list, aryTilesets, aryParallaxes) {
    list.filter(function (cmd) {
      return cmd.code === 282 || cmd.code === 284;
    }).forEach(function(cmd) {
      if (cmd.code === 282 && !aryTilesets.contains(cmd.parameters[0])) {
        aryTilesets.push(cmd.parameters[0]);
      }
      if (cmd.code === 284 && !aryParallaxes.contains(cmd.parameters[0])) {
        aryParallaxes.push(cmd.parameters[0]);
      }
    });
  };
})();