// ============================================
// Main Menu Var Shim
// YEP_MainMenuVarfix.js
// ============================================

//=============================================================================
/*:
 * @plugindesc v0.0.1 Adds an addtional check before displaying the extra menu.
 * @author Teven
 * 
 */
//============================================================================= 
Scene_Menu.prototype.createVariableWindow = function() {
    var x = this.getVariableWindowX();
    var y = this.getVariableWindowY();
    var w = this.getVariableWindowWidth();
    var h = this.getVariableWindowHeight();
    this._variableWindow = new Window_MainMenuVariable(x, y, w, h);
    if ($gameSwitches.value(11) == false){
      return;
    }else{
    this.addWindow(this._variableWindow);
  }
  };

  Window_MainMenuVariable.prototype.drawVariableData = function(i, x, y) {
    if (Yanfly.Param.MMVarId[i] <= 0) {
      return y;
    }
    var varId = Yanfly.Param.MMVarId[i];
    var name = $dataSystem.variables[varId];

    if (Imported.YEP_X_MoreCurrencies) {
      name = name.replace(/<<(.*?)>>/gi, '');
    }
    /* this.drawTextEx(name, x, y);
    var value = Yanfly.Util.toGroup($gameVariables.value(varId));
    var width = this.contents.width - this.textPadding() * 2;
    this.drawText(value, x, y, width, 'right');
    return y + this.lineHeight();
  }; */
    
    var value = Yanfly.Util.toGroup($gameVariables.value(varId));  
    var valspacing = ' '+ value;
    var namecoloring = '            \\C[16]'+ name;
    var width = this.contents.width - this.textPadding() * 2;
    this.drawText(valspacing, x, y, 'right');
    this.drawTextEx(namecoloring, x, y, width);
     
    // this.drawText(name, x, y, width, 'right');
    return y + this.lineHeight();
  };