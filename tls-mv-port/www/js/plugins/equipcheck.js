/*:
*
*@plugindesc Some new checks you can access!
*
*@author Fugama
*
*@help
*
* With these new identifiers you can customize
*Yanfly's lunatic mode just a little bit easier,
*or just have an easier time calling these checks for any other
*JS scripting you may be doing.
*
*
*The first command is "isEquippedWep()" it can be used like this
*"user.isEquippedWep(x)" where x is the weapon id You want to check!
*
*The second is "hasWep()". This checks if the Actor was any weapon at all
*(compatable with Dual Wielding) example:
*"user.hasWep()" this needs no paramaters for the parentheses.
*
*The third is "isEquippedArmor()" This checks for a specific Armor equip. example:
*"user.isEquippedArmor(x)" where x is the ID of the armor.
*
*Following the pattern, the fourth is "hasArmor()" This goes through and checks if the actor has any armor equipped!
*
*example: "user.hasArmor()"
*
*I hope you find some use from these ease-of-access addtions!
*/
(function(){


//Actor Checks
Game_Actor.prototype.isEquippedWep = function(WepID) {
var wep = $dataWeapons[WepID];
if (this.isActor() && this.equips().contains(wep)){
return true;
} else {
return false;
}
};


Game_Actor.prototype.hasWep = function() {

var equipList = this._equips;
var wepCount = 0;
for (i = 0; i < equipList.length; i++){
var itemCheck = equipList;
if (itemCheck._dataClass === "weapon"){
wepCount =+ 1;
}
}
if (wepCount != 0){
return true;
} else {
return false;
}
};

Game_Actor.prototype.isEquippedArmor = function(ArmID) {
var armor = $dataArmors[ArmID];
if (this.isActor() && this.equips().contains(armor)){
return true;
} else {
return false;
}
};


Game_Actor.prototype.hasArmor = function() {
var equipList = this._equips;
var ArmCount = 0;
for (i = 0; i < equipList.length; i++){
var itemCheck = equipList;
if (itemCheck._dataClass === "armor"){
ArmCount =+ 1;
}
}
if (ArmCount != 0){
return true;
} else {
return false;
}
};


//prevent crashes from enemy battlers using the same abilities
Game_BattlerBase.prototype.isEquippedWep = function(WepID) {
return false;
};


Game_BattlerBase.prototype.hasWep = function(WepID) {
return false;
};

Game_BattlerBase.prototype.hasArmor = function(WepID) {
return false;
};

Game_BattlerBase.prototype.isEquippedArmor = function(WepID) {
return false;
};



})();
