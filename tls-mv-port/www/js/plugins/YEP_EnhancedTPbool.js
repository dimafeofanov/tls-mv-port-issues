//=============================================================================
// Yanfly Engine Plugins - Enhanced TP equipment check shim
// YEP_EnhancedTPbool.js
//=============================================================================
//=============================================================================
 /*:
 * @plugindesc v0.1 Allows you to use eval inside the Preserve setting to allow
 * for additional customization of TP.  Place directly under YEP_EnhancedTP.js
 * Also requires you make an adjustment inside YEP_EnhancedTP.js on line 2669
 * Replace this text:
 * preserve: eval(String(Yanfly.Parameters['Mode ' + Yanfly.i + ' Preserve'])),
 * With this text:
 * preserve: String(Yanfly.Parameters['Mode ' + Yanfly.i + ' Preserve']),
 * Future versions I might be able to entirely shim without modifications to
 * source files.
 * @author Teve
 */ 
//=============================================================================
  
  $dataTpModes.forEach(function(tpMode, i){
  if (tpMode) {
    tpMode.preserve = Yanfly.Parameters[`Mode ${i} Preserve`]
  }
})

Game_BattlerBase.prototype.isPreserveTp = function() {
  const user = this
  if (this.tpMode()) return eval(this.tpMode().preserve)
  return Yanfly.ETP.Game_BattlerBase_isPreserveTp.call(this)
}
Game_Battler.prototype.regularRegenTp = function() {
  if ($gameParty.inBattle() && this.tpMode()) {  
  var value = this.getTpEval('regenTp', this, this, 0);
    this.gainTpmodeTp(value);
  }
  };
  // Add Maxing TP on recover all event
  Game_BattlerBase.prototype.recoverAll = function() {
    this.clearStates();
    this._hp = this.mhp;
    this._mp = this.mmp;
    this._tp = this.maxTp();
  };
  
  // Handler for slip damage kill stuff
  var SLIP_DEATH = 48;
  Game_Battler.prototype.maxSlipDamage = function() {
      if (this.isStateAffected(SLIP_DEATH)) {  
        return this.addState(1), Math.max(this.hp - 1, 0);  
      }else{
        return $dataSystem.optSlipDeath? this.hp : Math.max(this.hp - 1, 0);
     } 
};

// Game_Battler.prototype.onAllActionsEnd = function() {
//   this.clearResult();
//   this.removeStatesAuto(1);
//   //this.removeBuffsAuto();
// };

// Game_Battler.prototype.onTurnEnd = function() {
//   this.clearResult();
//   this.regenerateAll();
//   if (!BattleManager.isForcedTurn()) {
//       this.updateStateTurns();
//       this.updateBuffTurns();
//   }
//   this.removeStatesAuto(1);
//   this.removeBuffsAuto();
// }
Game_Action.prototype.itemEffectGainTp = function(target, effect) {
  var value = Math.floor(effect.value1);
  if (this.isItem()) {
    value *= this.subject().pha;
}  
  value = Math.floor(value)  
  if (value !== 0) {
      target.gainTp(value);
      this.makeSuccess(target);
    }
};

ITEM_EVENT_SWITCH_ID = 5
Game_Screen.prototype.onBattleStart = function() {
  this.clearFade();
  this.clearFlash();
  this.clearShake();
  this.clearZoom();
  this.eraseBattlePictures();
  $gameSwitches.setValue(ITEM_EVENT_SWITCH_ID,true)
};
Game_Battler.prototype.onBattleEnd = function() {
  this.clearResult();
  this.removeBattleStates();
  this.removeAllBuffs();
  this.clearActions();
  $gameSwitches.setValue(ITEM_EVENT_SWITCH_ID,false)
  if (!this.isPreserveTp()) {
      this.clearTp();
  }
  this.appear();
};
