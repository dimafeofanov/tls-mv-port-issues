//=============================================================================
// Shim for BattleAICore
// YEP_BattleAICoreshim.js
//=============================================================================
//=============================================================================
 /*:
 * @plugindesc v0.01 Shim to allow for direct targeting of actors based on tags
 * @author Teven
 *@help
 * 
 * Another Shim plugin.  This one is kind of dodgy honestly, as I didn't notice
 * the whopping 6 mobs that use AI functions until the 11th hour.  This one
 * Might have broken things, so keep an eye on the few AI controlled fights.
 * 
 * Like the Enhanced TP plugin this also required a minor edit to the base
 * YEP_BattleAICore.js.  If I get more motivated at some point I can probably
 * remove the need for that.
 */
Yanfly.CoreAI.DataManager_isDatabaseLoaded = DataManager.isDatabaseLoaded;
DataManager.isDatabaseLoaded = function() {
  if (!Yanfly.CoreAI.DataManager_isDatabaseLoaded.call(this)) return false;
  if (!Yanfly._loaded_YEP_BattleAICore) {
    this.processCoreAINotetags1($dataEnemies);
  	this.processCoreAINotetags2($dataSkills);
    this.processCoreAINotetags3($dataStates);
    this.processCoreAINotetags4($dataSystem);
    this.processCoreAINotetags5($dataActors);
    Yanfly._loaded_YEP_BattleAICore = true;
  }
	return true;
};
DataManager.processCoreAINotetags1 = function(group) {
    var note1 = /<(?:AI PRIORITY)>/i;
    var note2 = /<\/(?:AI PRIORITY)>/i;
    var note3 = /<(?:AI CONSIDER TAUNT|ai considers taunts)>/i;
    var note4 = /<(?:AI LEVEL):[ ](\d+)>/i;
    var note5 = /<(?:AI: target tag):[ ](\w+)>/i;;
    for (var n = 1; n < group.length; n++) {
          var obj = group[n];
          var notedata = obj.note.split(/[\r\n]+/);
  
      obj.aiPattern = [];
      var aiPatternFlag = false;
      obj.aiConsiderTaunt = false;
      obj.aiLevel = Yanfly.Param.CoreAIDefaultLevel * 0.01;
  
          for (var i = 0; i < notedata.length; i++) {
              var line = notedata[i];
              if (line.match(note1)) {
                  aiPatternFlag = true;
              } else if (line.match(note2)) {
          aiPatternFlag = false;
        } else if (aiPatternFlag) {
          obj.aiPattern.push(line);
        } else if (line.match(note3)) {
          obj.aiConsiderTaunt = true;
        } else if (line.match(note4)) {
          obj.aiLevel = parseFloat(RegExp.$1 * 0.01);
        }else if (line.match(note5)) {
            if (RegExp.$1 === 'fighter'){
              aiPatternFlag = true;
              obj.aiPattern.push(line);
              //console.log(line)
              //                atkfighter.push(obj.id)
            }else if (RegExp.$1 === 'healer'){
              aiPatternFlag = true;
              obj.aiPattern.push(line);
              //console.log(line)
              //                atkhealer.push(obj.id)
            }else if (RegExp.$1 === 'mage'){
              aiPatternFlag = true;
              obj.aiPattern.push(line);
              //console.log(line)
              //                atkmage.push(obj.id)
            }
        }
        
    }
    //console.log(obj.aiPattern)
      }
  };
var fighter = new Array;
var healer = new Array;
var mage = new Array;
DataManager.processCoreAINotetags5 = function(group) {
    var note1 = /<(?:AI_tag):[ ](\w+)>/i;
    for (var n = 1; n < group.length; n++) {
          var obj = group[n];
          var notedata = obj.note.split(/[\r\n]+/);
          for (var i = 0; i < notedata.length; i++) {
              var line = notedata[i];
              if (line.match(note1)) {
               if (RegExp.$1 === 'fighter'){
                    fighter.push(obj.id)
                }else if (RegExp.$1 === 'healer'){
                    healer.push(obj.id)
                }else if (RegExp.$1 === 'mage'){
                    mage.push(obj.id)
                }
              }
          }
      }
  };
  
  
  AIManager.isDecidedActionAI = function(line) {
    //console.log(line.id)
    //console.log(specialTarget)
    if (line.match(/<(?:AI: target tag):[ ](\w+)>/i)) {
      if (RegExp.$1 === "healer"){
        this._origCondition =  ("Always");
        var condition = ("Always");
        //console.log("match target healer")
        this._aiTarget = "heal";
        } else if (RegExp.$1 === "fighter"){
          this._origCondition =  ("Always");
          var condition = ("Always");
          this._aiTarget = "fight";
//          console.log("match target fighter")
      } else if (RegExp.$1 === "mage"){
        var condition = ("Always");
        this._origCondition =  ("Always");
        this._aiTarget = "cast";
  //      console.log("match target mage")
      }
    }else if (line.match(/[ ]*(.*):[ ](?:SKILL)[ ](\d+),[ ](.*)/i)) {
      this._origCondition =  String(RegExp.$1);
      var condition = String(RegExp.$1);
      this._aiSkillId = parseInt(RegExp.$2);
      this._aiTarget = String(RegExp.$3);
    } else if (line.match(/[ ]*(.*):[ ](?:SKILL)[ ](\d+)/i)) {
      this._origCondition =  String(RegExp.$1);
      var condition = String(RegExp.$1);
      this._aiSkillId = parseInt(RegExp.$2);
      this._aiTarget = 'RANDOM';
    } else if (line.match(/[ ]*(.*):[ ](.*),[ ](.*)/i)) {
      this._origCondition =  String(RegExp.$1);
      var condition = String(RegExp.$1);
      this._aiSkillId = Yanfly.SkillIdRef[String(RegExp.$2).toUpperCase()];
      this._aiTarget = String(RegExp.$3);
    } else if (line.match(/[ ]*(.*):[ ](.*)/i)) {
      this._origCondition =  String(RegExp.$1);
      var condition = String(RegExp.$1);
      this._aiSkillId = Yanfly.SkillIdRef[String(RegExp.$2).toUpperCase()];
      this._aiTarget = 'RANDOM';
    }else {
      return false;
    
    }

    if (!this.initialCheck(this._aiSkillId)) {
      var AI_Array = new Array;
      for (var i = 0; i < $dataSkills.length; i++) {
        if (this.hasSkill(i)){
          AI_Array.push(i)
        } 
         this._aiSkillId = AI_Array[Math.floor(Math.random() * AI_Array.length)];
         //this.action().setSkill(AI_Array);
      }; 
//      return true;
     }
    if (!this.meetCustomAIConditions(this._aiSkillId)) return false;
    this.action().setSkill(this._aiSkillId);
    if (!this.passAllAIConditions(condition)) return false;
    return true;
};
  
  function isMatch(value){
    // console.log("Value")
    // console.log(value)

    // console.log(type)

     for (var i = 0; i < type.length; i++) {
     var member = type[i]
    //  console.log("Member")
    //  console.log(member)
     if (value.actorId() === member) return true;

      }

    };

  
  AIManager.setProperTarget = function(group) {
    this.setActionGroup(group);
    var action = this.action();
    var randomTarget = group[Math.floor(Math.random() * group.length)];
    if (!randomTarget) return action.setTarget(0);
    if (group.length <= 0) return action.setTarget(randomTarget.index());
    var line = this._aiTarget.toUpperCase();
    if (line === "HEAL"){
      type = healer;
            var sgroup = (group.filter(isMatch));
            var sT = sgroup[Math.floor(Math.random() * sgroup.length)];
            var index = sgroup.indexOf();
      return action.setTarget(sT.index());
    }else if (line === "FIGHT"){
        type = fighter;
              var sgroup = (group.filter(isMatch))
              var sT = sgroup[Math.floor(Math.random() * sgroup.length)];
              var index = sgroup.indexOf();
        return action.setTarget(sT.index());
    } else if (line === "CAST"){
          type = mage;
                var sgroup = (group.filter(isMatch));
                var sT = sgroup[Math.floor(Math.random() * sgroup.length)];
                var index = sgroup.indexOf();
          return action.setTarget(sT.index());
    } else if (line.match(/FIRST/i)) {
      action.setTarget(0);
    } else if (line.match(/USER/i)) {
      var index = group.indexOf();
      action.setTarget(action.subject().index());
    } else if (line.match(/HIGHEST[ ](.*)/i)) {
      var param = this.getParamId(String(RegExp.$1));
      if (param < 0) return action.setTarget(randomTarget.index());
      if (param === 8) return this.setHighestHpFlatTarget(group);
      if (param === 9) return this.setHighestMpFlatTarget(group);
      if (param === 10) return this.setHighestHpRateTarget(group);
      if (param === 11) return this.setHighestMpRateTarget(group);
      if (param === 12) return this.setHighestLevelTarget(group);
      if (param === 13) return this.setHighestMaxTpTarget(group);
      if (param === 14) return this.setHighestTpTarget(group);
      if (param > 15) return action.setTarget(randomTarget.index());
      this.setHighestParamTarget(group, param);
    } else if (line.match(/LOWEST[ ](.*)/i)) {
      var param = this.getParamId(String(RegExp.$1));
      if (param < 0) return action.setTarget(randomTarget.index());
      if (param === 8) return this.setLowestHpFlatTarget(group);
      if (param === 9) return this.setLowestMpFlatTarget(group);
      if (param === 10) return this.setLowestHpRateTarget(group);
      if (param === 11) return this.setLowestMpRateTarget(group);
      if (param === 12) return this.setLowestLevelTarget(group);
      if (param === 13) return this.setLowestMaxTpTarget(group);
      if (param === 14) return this.setLowestTpTarget(group);
      if (param > 15) return action.setTarget(randomTarget.index());
      this.setLowestParamTarget(group, param);
    } else {
      this.setRandomTarget(group);
    }
};