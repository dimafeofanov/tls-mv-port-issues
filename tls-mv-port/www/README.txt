Please read the following list of controls that might not be obvious:

Shift: hold to walk instead of run, scrolls equip menu in shops
Ctrl, Q, or controller L1: auto-advance text (skips quickly when paired with instant text)
Arrow keys: scroll large text boxes
W key: switches between parties during certain segments

TLS can be played with a controller. The above buttons should correspond to controller buttons as assigned via F1.

F1: Options, remap keys
F5: enters or leaves full screen mode
Alt+Enter: alternate full screen mode (F5 is advised)
F6: change size of windowed display
F12: immediately restarts game




FREQUENTLY ASKED QUESTIONS
Q: Can save files be transferred between versions?
A: Yes. Simply move the save files - "Save01.rvdata2" and so on - into the game's new folder.

Q: How is experience distributed?
A: Any character currently in the party, alive or dead, active or inactive, will receive the same amount.

Q: How do relationship points work?
A: When your party members advise for/against an action, their points will change based on your decision.
   When characters reach 100, you can have a conversation with them that guarantees their loyalty and their relationship value will no longer lower.

Q: Is grinding possible?
A: No. Enemies and experience are finite quantities. You can grind certain slime enemies, however, that give items and little experience.

Q: Can Sx be converted into ProN?
A: No. It's meant to be spent on equipment without guilt.

Q: I'm stuck in Stineford, what do I do?
A: Try progressing the merchant plotline and sleeping.

Q: Attacks with multiple hits sometimes hit fewer times, are they bugged?
A: No. Multi-hit attacks select their targets at the beginning of the round, so if enemies are killed, some hits might be lost.



CREDITS
Created by: Sierra Lee

Huge thanks to: 
Decanter (for coding, debugging, and spriting)
Lone Wolf (for coding)
Bug Reporting (for spriting)
Scrambler (for spriting) 
Randomcouchpotato (for battlers)
Augustus Commodus (for maps)
Indoor Minotaur (for mapping)

Thanks for contributions from:
Lily Strife (music and misc resources)
RCP (misc resources)
Nytemare (misc resources)

Scripts:
Yanfly 
modern algebra 
Hime 
Shaz
Zeus81

Music:
Aaron Krogh
Audio Atelier
CAMeLIA
DJ Mixzilla
Enterbrain
G-MIYA
Jeremiah George
Joel Steudler
Kevin MacLeod
Mark McKeich
Sirkoto

Graphics: 
Luna 
Closet 
Mack 
Oni 
Thalzon 
Cyanyurikago
Tsukuru 
Sanwield
Raindrops Thanatos
Qut
Makapri
Vibrato
Chaos Portal/Mizusazanami
MikeCat
Dark Horseman
Ryo Mizuki
Sorejanai
Scinaya
Atachi Empire
Ruruga
Candacis
Shiro
Lunarea
Momope

Unofficial Build by Teven rider:
This build also uses plugins by:
Fugama
Victor Sant
DreamX
Casper Gaming
Galv
SumRndmDde
and there's a few by the people listed in the previous section too.

Some things to note:

If you're reading this via a text editor and not from the popup from the main menu, the link to Sierra's Patreon also
probably isn't working.  This is due to how nw.js handles security on file / link opening.  I swapped from a direct
url to a file with a link and increased the amount of my machines it works on from 1 to 3, so your mileage, etc.

Healing, Damage, Buff, and Debuff popups are a little different visually from VX Ace.  The MV version of
Yanfly's Battle Engine Core doesn't support display of Resist / Weak / Immune on damage.  I thought about writing an
extension of it but ultimately decided against it.  

The 3AW and the Ordeal of the Claw involve some trickery for group assembly.  I tested them pretty
extensively, but I'm just one guy, so its possible something is broken there.

As this was designed for my tablet, touch control was important.  As such, the two above mentioned events have a clickable
avatar in the upper lift corner when there's multiple parties.  Single finger touch should shift to the next party.
Also, double finger touch will spawn the main menu, as well as cancel a selection.

Battle AI is the only thing I have some concerns about, on account of realizing it was actually in use at the 11th hour.
By far and away not my finest work, but the mobs that use it seem to be mimicing their VX Ace counterparts.  Also, it
doesn't look like it broke all the mobs that don't use it, so I'll call it a success.

Autosave is a thing, and works the same as the Ace version, with one caveat: There's no restriction for saving to File 100.
It's also not labeled autosave.  If you're building to mobile, you might want to disable it via plugins if your device
is of the older variety.

Speaking of building to mobile, saving is quirky.  I'm pretty sure saves retain on removal / update, but I won't have the
time to devote to testing it for a while. In addition, if you get file errors on mobile builds(Maybe Linux/OSX too), check
for proper file casing.  I'm like 99% sure I sorted that out in my Android playtesting, but who knows if something will
screw itself up on archival / extraction.